# Dblyze

A MySQL analyzing library for PHP

- [Installation](#installation)
- [Usage](#usage)
- [Methods](#methods)

## Installation
Dblyze can be installed via [Composer](https://github.com/composer/composer).
```
$ composer require salby/dblyze
```

This will install dblyze as a dependency in a `vendor/` directory in your project.

## Usage

Use the autoloader in the `vendor/` directory to load the dblyze class.

```php
<?php

// Vendor autoload.
require_once(__DIR__ . '/vendor/autoload.php');

// Initialize the dbee class.
$db = new \salby\dbee\dbee([
    'host' => 'localhost',
    'database' => 'test',
    'user' => 'root',
    'password' => ''
]);

// Initialize the dblyze class.
$dblyze = new \salby\dblyze\Dblyze($db);

// Do stuff ...
```

## Methods

- [Get columns from table](#get-columns-from-table)
- [Find relations](#find-relations)
- [Analyze relational table](#analyze-relational-table)
- [Get all info about table](#get-all-info-about-table)
- [Check if table is a relational table](#check-if-table-is-a-relational-table)
- [Find column in table](#find-column-in-table)

### Get columns from table

```php
$columns = $dblyze -> columns('some_table')
```
Returns information about all columns in *some_table*.

### Find relations

```php
$relations = $dblyze -> relations([
    'TABLE_NAME' => 'user',
    'COLUMN_NAME' => 'city',
]);
```
Returns all columns *user*.*city* relates to.

```php
$relations = $dblyze -> relations([
    'REFERENCED_TABLE_NAME' => 'user',
    'COLUMN_NAME' => 'id',
]);
```
Returns all columns that relate to *user*.*id*.

#### All parameters you can search for
- **String** *TABLE_NAME*
- **String** *COLUMN_NAME*
- **String** *CONSTRAINT_NAME*
- **String** *REFERENCED_TABLE_NAME*
- **String** *REFERENCED_COLUMN_NAME*

### Ananlyze relational table

Get outer table information in a many-to-many relation e.g. `product <- product_category -> category`.

```php
$outer_table = $dblyze -> relational_table('product', 'product_category');
```

Returns an array that looks like:
```php
[
    'name' => 'category',
    'columns' => [...],
]
```
... where **name** contains the name of the outer table, and **columns** contains the columns found in the table.

### Get all info about table

Documentation coming soon.

### Check if table is a relational table

Documentation coming soon.

### Find column in table

Documentation coming soon.
