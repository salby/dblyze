<?php

namespace salby\dblyze;

use Exception;

class Dblyze
{

    protected $db;

    public function __construct($db)
    {
        $this -> db = $db;
    }

    /**
     * # Schema exists
     *
     * Checks if schema exists.
     *
     * @param string $schema
     *
     * @return bool
     */
    public function schema_exists($schema)
    {

        $sql = "SELECT SCHEMA_NAME
                  FROM INFORMATION_SCHEMA.SCHEMATA
                WHERE
                  SCHEMA_NAME = '$schema'";

        // Get list of schemas that match $schema.
        $schema_list = $this -> db -> fetch($sql);

        // Return false if schema doesn't exist, true if it does.
        return !empty($schema_list);

    }

    /**
     * # Tables
     *
     * Finds all tables in current schema.
     *
     * @param string $schema
     *
     * @return array
     */
    public function tables($schema = "SCHEMA()")
    {

        if ($schema !== "SCHEMA()") {

            // Throw exception if there is not schema named $schema.
            if (!$this->schema_exists($schema)) {
                throw new Exception("Tried to look in schema '$schema', but it doesn't exist.");
            } else {
                // Wrap $schema in quotes for use in SQL query.
                $schema = "'$schema'";
            }

        }

        $sql = "SELECT TABLE_NAME
                FROM INFORMATION_SCHEMA.TABLES
                WHERE
                  TABLE_TYPE = 'base table'
                  AND
                  TABLE_SCHEMA = $schema";

        // Create empty $tables array and insert names.
        $tables = [];
        foreach ($this->db->fetch($sql) as $table) {
            $tables[] = $table['TABLE_NAME'];
        }

        // Return pretty list of tables.
        return $tables;

    }

    /**
     * # Columns
     *
     * ## Example
     * ```php
     * $dblyze = new dblyze($db);
     * $dblyze -> columns('some_table')
     * ```
     * -- will return column data from __*some_table*__.
     *
     * @param string $table -- Table to analyze.
     *
     * @return array
     */
    public function columns($table)
    {

        // Fetch column data from $table and return array.
        $sql = "SHOW FULL COLUMNS FROM `$table`";
        return $this -> db -> fetch($sql);

    }

    /**
     * # Relations
     * Finds relational information from parameters given.
     *
     * ## Parameters
     *
     * **String** *TABLE_NAME*
     *
     * **String** *COLUMN_NAME*
     *
     * **String** *CONSTRAINT_NAME*
     *
     * **String** *REFERENCED_TABLE_NAME*
     *
     * **String** *REFERENCED_COLUMN_NAME*
     *
     * ## Example
     * ```php
     * $dblyze = new dblyze($db);
     * $dblyze -> relations([
     *     'TABLE_NAME' => 'some_table',
     *     'COLUMN_NAME' => 'some_column'
     * ]);
     * ```
     * -- returns relation data from constraint where table-name is __*some_table*__ and the column name is __*some_column*__.
     *
     * @param array $parameters
     *
     * @return array
     */
    public function relations($parameters)
    {

        $params = [];
        $sql = "SELECT TABLE_NAME,
                  COLUMN_NAME,
                  CONSTRAINT_NAME,
                  REFERENCED_TABLE_NAME,
                  REFERENCED_COLUMN_NAME
                  FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                WHERE
                  TABLE_SCHEMA = SCHEMA()";

        foreach ($parameters as $requirement => $value) {
            $params[$requirement] = $value;
            $sql .= " AND `$requirement` = :$requirement";
        }

        return $this -> db -> fetch($sql, $params);

    }

    /**
     * # Relational table
     * Finds table column data from outer relation via a relational table.
     *
     * @param $base_table
     * @param $relationalTable
     *
     * @return array
     */
    public function relationalTable($base_table, $relationalTable)
    {

        $columns = [];

        // Get relational-table relations.
        $rel_out = $this -> relations([
            'TABLE_NAME' => $relationalTable
        ]);

        // Exclude columns that relate to the primary key.
        $ignore = [];
        foreach ($rel_out as $rel) {
            if ($rel['REFERENCED_TABLE_NAME'] === $base_table)
                $ignore[] = $rel['COLUMN_NAME'];
        }

        // Build fake columns from relation end-point column (starting from base-table).
        foreach ($rel_out as $rel) {
            if ($rel['REFERENCED_TABLE_NAME'] !== $base_table) {

                // Build fake column.
                $column = [
                    'Field' => $rel['REFERENCED_TABLE_NAME'],
                    'Type' => null,
                    'Null' => '',
                    'Key' => 'MTM',
                    'Default' => null,
                    'Extra' => '',
                    'Columns' => [] // Insert column(s) here.
                ];

                // Find columns in relational-table.
                $relInfo = $this -> columns($relationalTable);

                // Insert column data into $column['Columns'] if not ignored.
                foreach ($relInfo as $rel_column) {
                    if (!in_array($rel_column['Field'], $ignore))
                        $column['Columns'][] = $rel_column;
                }
                $columns[] = $column;

            }
        }

        // Return info and fake columns.
        return [
            'name' => $relationalTable,
            'columns' => $columns,
        ];

    }

    /**
     * # Table info
     * Returns all column data from table.
     *
     * @param string $table
     * @param array $config
     *
     * @return array
     */
    public function tableInfo($table, $config = [])
    {
        $defaults = [
            'exclude' => []
        ];
        $config = array_merge($defaults, $config);

        // Get column data for base-table.
        $tableInfo = $this -> columns($table);

        // Get columns that relate to a column in base-table.
        $relIn = $this -> relations([
            'REFERENCED_TABLE_NAME' => $table
        ]);

        if (!empty($relIn)) {
            foreach ($relIn as $column) {

                $foreignTable = $column['TABLE_NAME'];

                // Skip iteration if table is excluded in config.
                if (in_array($foreignTable, $config['exclude']))
                    continue;

                // Get foreign table column data.
                $foreigntableInfo = $this -> columns($foreignTable);

                // Check if foreign table is a relational-table.
                $relTable = $this -> isRelationalTable($foreigntableInfo);

                // If there are no primary keys, assume foreign table
                // is a relational table.
                if ($relTable) {

                    // Get fake columns from relational-table.
                    $fakeColumns = $this -> relationalTable($table, $foreignTable)['columns'];

                    // Iterate and insert fake columns as normal columns.
                    foreach ($fakeColumns as $fk) {
                        $tableInfo[] = $fk;
                    }

                } else {

                    foreach ($foreigntableInfo as $foreignColumn) {

                        // Iterate and insert foreign columns if they aren't
                        // primary or foreign keys.
                        if ($foreignColumn['Key'] != 'PRI'
                            &&
                            $foreignColumn['Key'] != 'MUL')
                            $tableInfo[] = $foreignColumn;

                    }

                }

            }
        }

        // Return table column data.
        return $tableInfo;

    }

    /**
     * # Is relational table.
     * Checks if table contains primary keys, and if not,
     * assume it's a relational table.
     *
     * @param mixed $table - Table to analyze. Takes either an array of columns or a table-name.
     *
     * @return bool
     */
    public function isRelationalTable($table)
    {

        $columns = is_array($table)
            ? $table
            : $this -> columns($table);

        $pri = [];
        foreach ($columns as $column) {
            if ($column['Key'] === 'PRI')
                $pri[] = $column;
        }

        return count($pri) === 0;

    }

    /**
     * # Finds single column in table from criteria.
     *
     * ## Config
     *
     * **Array** *find*
     *
     * **Array** *exclude*
     *
     * ### Properties to check
     *
     * **String** *Field* -- Column name.
     *
     * **String** *Type*
     *
     * **String** *Collation*
     *
     * **String** *Null* -- Defines if *NULL* is allowed
     *
     * **String** *Key* -- Key, e.g. *'PRI'* for primary key, *'MUL'* for foreign key etc.
     *
     * **String** *Default*
     *
     * **String** *Extra*
     *
     * **String** *Privileges*
     *
     * **String** *Comment* -- Column comment.
     *
     * @param $columns - Columns to search in. Can be a table-name or array of columns.
     * @param array $config
     *
     * @return array
     */
    public function findColumn($columns, $config = [ 'exclude' => [ 'Key' => ['PRI', 'MUL'] ] ])
    {

        // Get columns if table-name is given.
        if (!is_array($columns)) {
            $columns = $this -> columns($columns);
        }

        // Search in columns.
        foreach ($columns as $column) {

            // Doesn't match by default.
            $theOne = false;

            // Check if column contains must-have values if set.
            if (isset($config['find'])) {
                $theOne = columnMatch('find', $column, $config['find']);
            }

            // Check if column doesn't contain excluded values if set.
            if (isset($config['exclude'])) {
                $theOne = columnMatch('exclude', $column, $config['exclude']);
            }

            // Break loop if column matches given criteria.
            if ($theOne) {
                $found = $column;
                break;
            }

        }

        // Return column if found, else return empty array.
        return isset($found)
            ? $found
            : [];

    }

}

function columnMatch($behaviour, $column, $values, $criterea = "")
{
    $matches = 0;
    foreach ($values as $req => $val) {
        if (empty($criterea))
            $criterea = $req;
        if (is_array($val)) {
            if (columnMatch($behaviour, $column, $val, $criterea))
                $matches++;
        } else {
            if ($behaviour === 'find') {
                if ($column[$criterea] === $val)
                    $matches++;
            } else if ($behaviour === 'exclude') {
                if ($column[$criterea] !== $val)
                    $matches++;
            }
        }
    }
    return $matches === count($values);
}
